package com.mrp.servicecuenta.util;

public class ConstUtil {

    public static final String MSJE_EXITO = "Proceso ejecutado con éxito";

    public static final String MSJE_ERROR = "Proceso ejectuado con errores";

    public static final Integer CUOTA_1 = 1;

    public static final String METODO_PAGO_EFECTIVO = "E";
    public static final String METODO_PAGO_CREDITO = "C";

    public static final String CODIGO_CONCEPTO_VENTA = "3";

    public static final String ACEPTADO = "ACEPTADO";
    public static final String CAJA_ABIERTA = "ABIERTA";
    public static final String CAJA_CERRADA = "CERRADA";
    public static final String INGRESO = "I";
    public static final String EGRESO = "E";
    public static final String METODO_PAGO_CODIGO_CREDITO = "3";

    public static final String CODIGO_CAJA_ABIERTA = "1";
    public static final String CODIGO_CAJA_CERRADA = "2";

    public static final String CODIGO_COMPROBANTE_TICKET = "98";
    public static final String CODIGO_COMPROBANTE_BOLETA = "03";
    public static final String CODIGO_COMPROBANTE_FACTURA = "01";

    public static final String MENSAJE_VENTA_EFECTIVO="Venta en efectivo documento de venta ";
    public static final String MENSAJE_VENTA_CREDITO="Venta a credito documento de venta ";

    public static final String DOMINIO_CORREO ="@zapopam.com.mx";
    public static final String BASE_CLIENTE ="CLI";
    public static final String BASE_USUARIO ="USU";

    public static final String BASE_CATALOGO_PRECIO ="CLP";

    public static final String BASE_LISTA_PRECIO ="LTP";

    public static final String BASE_CAJA ="CAJ";

    public static final String BASE_CATEGORIA ="CAT";

    public static final String BASE_MARCA ="MAR";

    public static final String BASE_MEDICO ="MED";
    public static final String  BASE_PRODUCTO ="PRO";

    public static final String BASE_CONCEPTO ="MAR";

    public static final String CODIGO_TIPO_DOCUMENTO_INICIAL="1";

    public static final String CODIGO_ROL_CLIENTE = "ROLE_CLIENTE";
    public static final String CODIGO_ROL_MEDICO = "ROLE_MEDICO";

    //REPORTES
    public static final String TITULO = "titulo";
    public static final String LOGO = "logo";

    public static final String ESTADO_VENTA_REGISTRADA = "REGISTRADO";

    //PARAMETROS REPORTES
    public static final String PARAMETRO_QR = "qr";
    public static final String PARAMETRO_EMPRESA = "empresa";
    public static final String PARAMETRO_VENTA = "venta";
    public static final String PARAMETRO_TOTAL_LETRA = "totalLetras";
    public static final String PARAMETRO_OBSERVACIONES = "observaciones";

    public static final String CONTENT_TYPE_PDF = "application/pdf";

    public static final String A4 = "A4";

    public static final String TICKET80 = "ticket80";

    //CONSTANTES DE PROCESAMIENTO BATCH
    public static final String ESTADO_INICIAL = "INICIAL";

    public static final String ESTADO_PROCESANDO = "PROCESANDO";

    public static final String ESTADO_SINCRONIZADO = "SINCRONIZADO";

    public static final String ESTADO_CONVERSION = "CONVERSION";

    public static final String ESTADO_FALLIDO = "FALLIDO";

    public static final String ESTADO_FINALIZADO = "FINALIZADO";

    public static final String ESTADO_TRABAJANDO = "TRABAJANDO";

    public static final String CATALOGO_PRODUCTOS = "PRODUCTOS";
    public static final String CATALOGO_DESCUENTOS = "DESCUENTOS";
    public static final String CATALOGO_CLIENTES= "CLIENTES";
    public static final String CATALOGO_PRECIOS = "PRECIOS";

    public static final String AUDITORIA_USUARIO = "USER_BATCH";

    public static final String URI_T3 = "http://localhost:8095/api";
    public static final String URI_T3_SINCRONIZAR_SUCURSAL = "/batch/sincronizar/sucursal";





}
