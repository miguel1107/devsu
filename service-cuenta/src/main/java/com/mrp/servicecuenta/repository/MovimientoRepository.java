package com.mrp.servicecuenta.repository;

import com.app.commondb.entity.Movimiento;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface MovimientoRepository extends PagingAndSortingRepository<Movimiento, UUID>, JpaSpecificationExecutor<Movimiento> {
    static Specification<Movimiento> isEnable(Boolean enable) {
        return (p, q, b) ->
                b.equal(p.get("indDel"), enable);
    }

    Optional<Movimiento> findByNumeroCuentaAndIndDelIsFalse(String codigo);
}
