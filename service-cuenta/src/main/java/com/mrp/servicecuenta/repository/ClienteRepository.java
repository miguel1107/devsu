package com.mrp.servicecuenta.repository;

import com.app.commondb.entity.Cliente;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, UUID>, JpaSpecificationExecutor<Cliente> {

    static Specification<Cliente> isEnable(Boolean enable) {
        return (p, q, b) ->
                b.equal(p.get("indDel"), enable);
    }

    Optional<Cliente> findBy(String codigo);

}
