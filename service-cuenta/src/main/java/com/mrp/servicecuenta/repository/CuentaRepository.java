package com.mrp.servicecuenta.repository;

import com.app.commondb.entity.Cuenta;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface CuentaRepository extends PagingAndSortingRepository<Cuenta, UUID>, JpaSpecificationExecutor<Cuenta> {

    static Specification<Cuenta> isEnable(Boolean enable) {
        return (p, q, b) ->
                b.equal(p.get("indDel"), enable);
    }

    Optional<Cuenta> findByNumeroCuentaAndIndDelIsFalse(String codigo);

}
