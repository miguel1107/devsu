package com.mrp.servicecuenta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceCuentaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceCuentaApplication.class, args);
	}

}
