package com.mrp.servicecuenta.service.inter;

import com.app.commondb.entity.Cuenta;
import com.app.commondb.model.CuentaDTO;
import com.app.commondb.model.ResponseHttpUtil;
import com.mrp.servicecuenta.exception.UnprocessableEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ICuentaService {

    public List<CuentaDTO> findAll(Specification<Cuenta> specs, Sort sort);

    public Page<CuentaDTO> findAll(Specification<Cuenta> specs, Pageable pageable);

    public Optional<CuentaDTO> findById(UUID key);

    public CuentaDTO mapToClassDTO(Cuenta cuenta);

    public Cuenta mapToClass(CuentaDTO cuenta);

    public ResponseHttpUtil createAdvance(CuentaDTO cuenta) throws UnprocessableEntityException;
    public ResponseHttpUtil updateAdvance(CuentaDTO cuenta) throws UnprocessableEntityException;

    public ResponseHttpUtil deleteAdvance(CuentaDTO cuenta) throws UnprocessableEntityException;



}
