package com.mrp.servicecuenta.service.impl;

import com.app.commondb.entity.Cliente;
import com.app.commondb.entity.Cuenta;
import com.app.commondb.model.ClienteDTO;
import com.app.commondb.model.CuentaDTO;
import com.app.commondb.model.ResponseHttpUtil;
import com.google.common.base.Strings;
import com.mrp.servicecuenta.exception.EnumErrores;
import com.mrp.servicecuenta.exception.ErrorMessage;
import com.mrp.servicecuenta.exception.UnprocessableEntityException;
import com.mrp.servicecuenta.repository.ClienteRepository;
import com.mrp.servicecuenta.repository.CuentaRepository;
import com.mrp.servicecuenta.service.inter.IClienteService;
import com.mrp.servicecuenta.util.ConstUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClienteServiceImpl implements IClienteService {

    private final ModelMapper modelMapper;
    private final ClienteRepository clienteRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ClienteDTO> findAll(Specification<Cliente> specs, Sort sort) {
        log.info("Inicio - ClienteServiceImpl - findAllList: ");
        Cliente cliente = new Cliente();
        cliente.setIndDel(false);
        if (specs == null) specs = Specification.where(null);
        if (buildWhereSpec(cliente) != null) specs = specs.and(buildWhereSpec(cliente));
        return clienteRepository.findAll(specs, sort).stream()
                .map(this::mapToClassDTO).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ClienteDTO> findAll(Specification<Cliente> specs, Pageable pageable) {
        log.info("Inicio - ClienteServiceImpl - findAllPage: ");
        Cliente cliente = new Cliente();
        cliente.setIndDel(false);
        if (specs == null) specs = Specification.where(null);
        if (buildWhereSpec(cliente) != null) specs = specs.and(buildWhereSpec(cliente));
        return clienteRepository.findAll(specs, pageable)
                .map(this::mapToClassDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ClienteDTO> findById(UUID key) {
        log.info("Inicio - ClienteServiceImpl - findById: " + key);
        return  clienteRepository.findById(key).map(this::mapToClassDTO);
    }

    @Override
    public ClienteDTO mapToClassDTO(Cliente cliente) {
        return modelMapper.map(cuenta, ClienteDTO.class);
    }

    @Override
    public Cuenta mapToClass(ClienteDTO clienteDTO) {
        return modelMapper.map(clienteDTO, Cliente.class);
    }

    private Specification<Cliente> buildWhereSpec(Cliente cliente) {
        Specification<Cliente> specification = Specification.where(null);
        if (cliente.getIndDel() != null)
            specification = specification.and(CuentaRepository.isEnable(cuenta.getIndDel()));
        return specification;
    }

    @Override
    @Transactional( rollbackFor = Exception.class)
    public ResponseHttpUtil createCliente(ClienteDTO clienteDTO) throws UnprocessableEntityException {
        log.info("Inicio - ClienteServiceImpl - createAdvance: ");
        Cliente clienteNew = null;
        ResponseHttpUtil responseConnection = new ResponseHttpUtil();

        validarRequest(clienteDTO);
        clienteNew = mapToClass(clienteDTO);
        //cuentaNew.setCodigo(codigo);
        clienteNew.setIndDel(false);
        Cliente cuentaSave = clienteRepository.save(clienteNew);

        responseConnection.setIdRequest(cuentaSave.getKey());
        responseConnection.setSuccess(true);
        responseConnection.setCode(HttpStatus.ACCEPTED.value());
        responseConnection.setData(mapToClassDTO(clienteNew));
        List<String> mensajes = new ArrayList<>();
        mensajes.add(ConstUtil.MSJE_EXITO);
        responseConnection.setMessages(mensajes);

        return responseConnection;
    }

    @Override
    @Transactional( rollbackFor = Exception.class)
    public ResponseHttpUtil updateCliente(ClienteDTO clienteDTO) throws UnprocessableEntityException {
        log.info("Inicio - ClienteServiceImpl - updateAdvance: ");
        ResponseHttpUtil responseConnection = new ResponseHttpUtil();

        validarRequest(clienteDTO);

        UUID valueUUID = UUID.fromString(clienteDTO.getKey());
        Optional<Cuenta> clienteSearch = clienteRepository.findById(valueUUID);

        if(clienteSearch.isEmpty())      {
            List<ErrorMessage> listaErrores = new ArrayList<>();
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1003.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1003.getMensaje()));
            throw new UnprocessableEntityException(listaErrores);
        }
        //agregar los que faltan
        Cliente clienteOld = clienteSearch.get();
        clienteOld.setNumeroCuenta(clienteDTO.getPassword()));
        clienteOld.setUpdatedBy(clienteDTO.g());
        Cuenta cuentaUpdate = clienteRepository.save(cuentaOld);

        responseConnection.setIdRequest(cuentaUpdate.getKey());
        responseConnection.setSuccess(true);
        responseConnection.setCode(HttpStatus.ACCEPTED.value());
        responseConnection.setData(mapToClassDTO(cuentaUpdate));
        List<String> mensajes = new ArrayList<>();
        mensajes.add(ConstUtil.MSJE_EXITO);
        responseConnection.setMessages(mensajes);
        responseConnection.setMessages(mensajes);
        return responseConnection;
    }


    private void validarRequest(ClienteDTO clienteDTO) {
        List<ErrorMessage> listaErrores = new ArrayList<>();

        if (!Strings.isNullOrEmpty(clienteDTO.getKey())){
            try {
                UUID value = UUID.fromString(clienteDTO.getKey());
            } catch (IllegalArgumentException e){
                listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1004.getCodigo(),
                        EnumErrores.ERROR_EXCEPCION_1004.getMensaje()));
            }
        }

        /* if (Strings.isNullOrEmpty(clienteDTO.getNumerCuenta())){
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1002.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1002.getMensaje()));
        } */


        if (!listaErrores.isEmpty()) {
            throw new UnprocessableEntityException(listaErrores);
        }
    }
}
