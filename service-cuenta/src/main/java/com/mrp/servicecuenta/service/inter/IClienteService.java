package com.mrp.servicecuenta.service.inter;

import com.app.commondb.entity.Cliente;
import com.app.commondb.entity.Cuenta;
import com.app.commondb.model.ClienteDTO;
import com.app.commondb.model.ResponseHttpUtil;
import com.mrp.servicecuenta.exception.UnprocessableEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IClienteService {

    public List<ClienteDTO> findAll(Specification<Cliente> specs, Sort sort);

    public Page<ClienteDTO> findAll(Specification<Cliente> specs, Pageable pageable);

    public Optional<ClienteDTO> findById(UUID key);

    public ClienteDTO mapToClassDTO(Cliente cliente);

    public Cliente mapToClass(ClienteDTO cliente);

    public ResponseHttpUtil createAdvance(ClienteDTO cliente) throws UnprocessableEntityException;
    public ResponseHttpUtil updateAdvance(ClienteDTO cliente) throws UnprocessableEntityException;

    public ResponseHttpUtil deleteAdvance(ClienteDTO cliente) throws UnprocessableEntityException;
}
