package com.mrp.servicecuenta.service.impl;

import com.app.commondb.entity.Cuenta;
import com.app.commondb.model.CuentaDTO;
import com.app.commondb.model.ResponseHttpUtil;
import com.google.common.base.Strings;
import com.mrp.servicecuenta.exception.EnumErrores;
import com.mrp.servicecuenta.exception.ErrorMessage;
import com.mrp.servicecuenta.exception.UnprocessableEntityException;
import com.mrp.servicecuenta.repository.CuentaRepository;
import com.mrp.servicecuenta.service.inter.ICuentaService;
import com.mrp.servicecuenta.util.ConstUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CuentaServiceImpl implements ICuentaService {
    
    private final ModelMapper modelMapper;
    private final CuentaRepository cuentaRepository;

    @Override
    @Transactional(readOnly = true)
    public List<CuentaDTO> findAll(Specification<Cuenta> specs, Sort sort) {
        log.info("Inicio - CuentaServiceImpl - findAllList: ");
        Cuenta cuenta = new Cuenta();
        cuenta.setIndDel(false);
        if (specs == null) specs = Specification.where(null);
        if (buildWhereSpec(cuenta) != null) specs = specs.and(buildWhereSpec(cuenta));
        return cuentaRepository.findAll(specs, sort).stream()
                .map(this::mapToClassDTO).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CuentaDTO> findAll(Specification<Cuenta> specs, Pageable pageable) {
        log.info("Inicio - CuentaServiceImpl - findAllPage: ");
        Cuenta cuenta = new Cuenta();
        cuenta.setIndDel(false);
        if (specs == null) specs = Specification.where(null);
        if (buildWhereSpec(cuenta) != null) specs = specs.and(buildWhereSpec(cuenta));
        return cuentaRepository.findAll(specs, pageable)
                .map(this::mapToClassDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CuentaDTO> findById(UUID key) {
        log.info("Inicio - CuentaServiceImpl - findById: " + key);
        return cuentaRepository.findById(key).map(this::mapToClassDTO);
    }

    @Override
    public CuentaDTO mapToClassDTO(Cuenta cuenta) {
        return modelMapper.map(cuenta, CuentaDTO.class);
    }

    @Override
    public Cuenta mapToClass(CuentaDTO cuentaDTO) {
        return modelMapper.map(cuentaDTO, Cuenta.class);
    }

    private Specification<Cuenta> buildWhereSpec(Cuenta cuenta) {
        Specification<Cuenta> specification = Specification.where(null);
        if (cuenta.getIndDel() != null)
            specification = specification.and(CuentaRepository.isEnable(cuenta.getIndDel()));
        return specification;
    }


    @Override
    @Transactional( rollbackFor = Exception.class)
    public ResponseHttpUtil createAdvance(CuentaDTO cuentaDTO) throws UnprocessableEntityException {
        log.info("Inicio - CuentaServiceImpl - createAdvance: ");
        Cuenta cuentaNew = null;
        ResponseHttpUtil responseConnection = new ResponseHttpUtil();

        validarRequest(cuentaDTO);
        cuentaNew = mapToClass(cuentaDTO);
        //cuentaNew.setCodigo(codigo);
        cuentaNew.setIndDel(false);
        Cuenta cuentaSave = cuentaRepository.save(cuentaNew);

        responseConnection.setIdRequest(cuentaSave.getKey());
        responseConnection.setSuccess(true);
        responseConnection.setCode(HttpStatus.ACCEPTED.value());
        responseConnection.setData(mapToClassDTO(cuentaNew));
        List<String> mensajes = new ArrayList<>();
        mensajes.add(ConstUtil.MSJE_EXITO);
        responseConnection.setMessages(mensajes);

        return responseConnection;
    }

    @Override
    @Transactional( rollbackFor = Exception.class)
    public ResponseHttpUtil updateAdvance(CuentaDTO cuentaDTO) throws UnprocessableEntityException {
        log.info("Inicio - CuentaServiceImpl - updateAdvance: ");
        ResponseHttpUtil responseConnection = new ResponseHttpUtil();

        validarRequest(cuentaDTO);

        UUID valueUUID = UUID.fromString(cuentaDTO.getKey());
        Optional<Cuenta> cuentaSearch = cuentaRepository.findById(valueUUID);

        if(cuentaSearch.isEmpty())      {
            List<ErrorMessage> listaErrores = new ArrayList<>();
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1003.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1003.getMensaje()));
            throw new UnprocessableEntityException(listaErrores);
        }
        //agregar los que faltan
        Cuenta cuentaOld = cuentaSearch.get();
        cuentaOld.setNumeroCuenta(cuentaDTO.getNumerCuenta());
        cuentaOld.setUpdatedBy(cuentaDTO.getTipoCuenta());
        Cuenta cuentaUpdate = cuentaRepository.save(cuentaOld);

        responseConnection.setIdRequest(cuentaUpdate.getKey());
        responseConnection.setSuccess(true);
        responseConnection.setCode(HttpStatus.ACCEPTED.value());
        responseConnection.setData(mapToClassDTO(cuentaUpdate));
        List<String> mensajes = new ArrayList<>();
        mensajes.add(ConstUtil.MSJE_EXITO);
        responseConnection.setMessages(mensajes);
        responseConnection.setMessages(mensajes);
        return responseConnection;
    }

    @Override
    @Transactional( rollbackFor = Exception.class)
    public ResponseHttpUtil deleteAdvance(CuentaDTO cuentaDTO) throws UnprocessableEntityException {
        log.info("Inicio - CuentaServiceImpl - updateAdvance: ");
        ResponseHttpUtil responseConnection = new ResponseHttpUtil();

        validarRequest(cuentaDTO);

        UUID valueUUID = UUID.fromString(cuentaDTO.getKey());
        Optional<Cuenta> cuentaSearch = cuentaRepository.findById(valueUUID);

        if(cuentaSearch.isEmpty())      {
            List<ErrorMessage> listaErrores = new ArrayList<>();
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1003.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1003.getMensaje()));
            throw new UnprocessableEntityException(listaErrores);
        }

        Cuenta cuentaOld = cuentaSearch.get();
        cuentaOld.setIndDel(true);
        Cuenta cuentaUpdate = cuentaRepository.save(cuentaOld);

        responseConnection.setIdRequest(cuentaUpdate.getKey());
        responseConnection.setSuccess(true);
        responseConnection.setCode(HttpStatus.ACCEPTED.value());
        responseConnection.setData(mapToClassDTO(cuentaUpdate));
        List<String> mensajes = new ArrayList<>();
        mensajes.add(ConstUtil.MSJE_EXITO);
        responseConnection.setMessages(mensajes);
        responseConnection.setMessages(mensajes);
        return responseConnection;
    }


    private void validarRequest(CuentaDTO cuentaDTO) throws UnprocessableEntityException {
        List<ErrorMessage> listaErrores = new ArrayList<>();

        if (!Strings.isNullOrEmpty(cuentaDTO.getKey())){
            try {
                UUID value = UUID.fromString(cuentaDTO.getKey());
            } catch (IllegalArgumentException e){
                listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1004.getCodigo(),
                        EnumErrores.ERROR_EXCEPCION_1004.getMensaje()));
            }
        }

        if (Strings.isNullOrEmpty(cuentaDTO.getNumerCuenta())){
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1002.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1002.getMensaje()));
        }


        if (!listaErrores.isEmpty()) {
            throw new UnprocessableEntityException(listaErrores);
        }
    }
}
