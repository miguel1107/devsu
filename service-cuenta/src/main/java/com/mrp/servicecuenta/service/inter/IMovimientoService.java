package com.mrp.servicecuenta.service.inter;

import com.app.commondb.entity.Movimiento;
import com.app.commondb.model.MovimienDTO;
import com.app.commondb.model.ResponseHttpUtil;
import com.mrp.servicecuenta.exception.UnprocessableEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IMovimientoService {

    public List<MovimienDTO> findAll(Specification<Movimiento> specs, Sort sort);

    public Page<MovimienDTO> findAll(Specification<Movimiento> specs, Pageable pageable);

    public Optional<MovimienDTO> findById(UUID key);

    public MovimienDTO mapToClassDTO(Movimiento movimiento);

    public Movimiento mapToClass(MovimienDTO movimienDTO);

    public ResponseHttpUtil createAdvance(MovimienDTO movimienDTO) throws UnprocessableEntityException;
    public ResponseHttpUtil updateAdvance(MovimienDTO movimienDTO) throws UnprocessableEntityException;

    public ResponseHttpUtil deleteAdvance(MovimienDTO movimienDTO) throws UnprocessableEntityException;
}
