package com.mrp.servicecuenta.exception;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


@Getter
@Setter
public class ErrorResponse {

    @JsonProperty("success")
    private Boolean success;

    @JsonProperty("code")
    private Integer code;

    @JsonProperty("idRequest")
    private UUID idRequest;

    @JsonProperty("messages")
    private List<ErrorMessage> messages;

    @JsonProperty("data")
    private List<Object> data;

}
