package com.mrp.servicecuenta.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UnprocessableEntityException extends RuntimeException  {
    private static final long serialVersionUID = -3473445221002823995L;
    private List<ErrorMessage> errors;
    public UnprocessableEntityException() {
        errors = new ArrayList<>();
    }

    public UnprocessableEntityException(List<ErrorMessage> errors) {
        this.errors = errors;
    }
    public UnprocessableEntityException(ErrorMessage... errors) {
        this.errors = Arrays.asList(errors);
    }
    public UnprocessableEntityException addError(String desError) {
        ErrorMessage errorDetail = new ErrorMessage( "999", desError);
        errors.add(errorDetail);
        return this;
    }
    public UnprocessableEntityException addError( String codError, String desError) {
        ErrorMessage errorDetail = new ErrorMessage( codError, desError);
        errors.add(errorDetail);
        return this;
    }
    public UnprocessableEntityException addError(EnumErrores errorEnum) {
        ErrorMessage errorDetail = new ErrorMessage( errorEnum.getCodigo(), errorEnum.getMensaje());
        errors.add(errorDetail);
        return this;
    }

    public List<ErrorMessage> getErrors() {
        return errors;
    }
    public void setErrors(List<ErrorMessage> errors) {
        this.errors = errors;
    }
}