package com.mrp.servicecuenta.exception;


public enum EnumErrores {

    ERROR_EXCEPCION_1001("1001", "Error a escribir."),
    ERROR_EXCEPCION_1002("1002", "Ingrese un numero de cuenta."),
    ERROR_EXCEPCION_1003("1003", "No se encuentra el registro."),
    ERROR_EXCEPCION_1004("1004", "UUID con error"),
    ERROR_EXCEPCION_1005("1005", "Ingrese un cliente registrado"),
    ERROR_EXCEPCION_1006("1006", "Debe seleccionar al menos un producto para la venta."),
    ERROR_EXCEPCION_1007("1007", "Aperturar caja antes de realizar una venta."),
    ERROR_EXCEPCION_1008("1008", "Debe seleccione un tipo de comprobante."),
    ERROR_EXCEPCION_1009("1009", "No existe stock valido para el producto: "),
    ERROR_EXCEPCION_1010("1010", "Error no se encuentra la caja. "),
    ERROR_EXCEPCION_1011("1011", "Error la caja se encuentra abierta. "),
    ERROR_EXCEPCION_1012("1012", "Ingrese nombre del cliente. "),
    ERROR_EXCEPCION_1013("1013", "Ingrese apellidos del cliente. "),
    ERROR_EXCEPCION_1014("1014", "Ingrese número de documento del cliente. "),
    ERROR_EXCEPCION_1015("1015","Ingrese los datos principales del usuario. "),
    ERROR_EXCEPCION_1016("1016", "Ingrese un numero de avisos."),
    ERROR_EXCEPCION_1017("1017", "Ingrese un monto."),

    ERROR_EXCEPCION_1018("1018", "La Venta no existe."),
    ERROR_EXCEPCION_1019("1019", "Error al generar el comprobante."),

    ERROR_EXCEPCION_1029("1029", "Error al subir al subir archivo"),

    ERROR_EXCEPCION_1030("1030", "El archivo no puede estar vacío"),
    ERROR_EXCEPCION_1031("1031", "Catalogo invalido"),

    ERROR_EXCEPCION_1032("1032", "El catalogo que desea sincronizar no existe"),

    ERROR_EXCEPCION_1033("1033", "No hay ningun registro pendiente de procesar"),

    ERROR_EXCEPCION_1034("1034", "Error al invocar el proceso batch"),

    ERROR_EXCEPCION_1035("1035", "Error al cargar archivo"),

    ERROR_EXCEPCION_1036("1036", "Error cliente T3");





    private EnumErrores(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCodigo() {
        return code;
    }

    public String getMensaje() {
        return msg;
    }

    public static String getMensaje(String code) {
        String msg = "";
        for (EnumErrores enumHTTP : EnumErrores.values()) {
            if (enumHTTP.code.equalsIgnoreCase(code)) {
                msg = enumHTTP.msg;
                break;
            }
        }
        return msg;
    }
}
