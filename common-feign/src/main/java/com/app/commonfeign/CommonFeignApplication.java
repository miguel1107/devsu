package com.app.commonfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EntityScan("com.app.commondb")
@EnableFeignClients
public class CommonFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonFeignApplication.class, args);
    }

}
