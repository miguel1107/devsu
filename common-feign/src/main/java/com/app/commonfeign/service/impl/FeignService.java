package com.app.commonfeign.service.impl;

import com.app.commondb.entity.Sucursal;
import com.app.commondb.entity.Usuario;
import com.app.commondb.model.*;
import com.app.commondb.util.EnumErrores;
import com.app.commonfeign.service.inter.SincronizarControllerFeign;
import com.app.commonfeign.service.inter.SucursalControllerFeign;
import com.app.commonfeign.service.inter.UsuarioControllerFeign;
import com.app.commonfeign.utils.ErrorMessage;
import com.app.commonfeign.utils.UnprocessableEntityException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import feign.RetryableException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class FeignService {

    @Autowired
    private UsuarioControllerFeign usuarioControllerFeign;

    @Autowired
    private SucursalControllerFeign sucursalControllerFeign;

    @Autowired
    private SincronizarControllerFeign sincronizarControllerFeign;



    /**
     * Envia una excepcion general para todos los FeignController que respondan con un FeignException
     * el mensaje de error es convertido a ErrorDTO el cual tiene el campo Message que sera devuelto en
     * un string dentro de la excepcion. El objeto ErrorDTO solo es utilizado para capturar el JSON de respuesta
     * del servicio.<br>
     * En caso de que sea la excepcion con HttpStatus.CONTINUE hara un throw Exception.
     *
     * @param e FeignException del FeignController
     * @return Exception con el mensaje que se extrajo con ErrorDTO
     * @throws Exception
     */
    private Exception exception(FeignException e) throws Exception {
        log.error(e);
        if (e instanceof RetryableException)
            return new Exception(e.getMessage());
        if (e.status() == HttpStatus.CONTINUE.value())
            throw new Exception(e.getMessage());
        ErrorDTO errorDTO = new ObjectMapper().readValue(e.contentUTF8(), ErrorDTO.class);
        return new Exception(errorDTO.getError().toString());
    }

    /**
     * AccessControllerFeign.getUserByEmail<br>
     * Obtiene el usuario por su correo electronico
     *
     * @param email del usuario a buscar
     * @return Usuario.class que resulta de la busqueda
     * @throws Exception Lanza una excepcion en caso de no encontrar el usuario o no se llegara a establecer conexion
     */
    public Usuario getUserByEmail(String email) throws Exception {
        try {
            return usuarioControllerFeign.getUserByEmail(email);
        } catch (FeignException e) {
            throw exception(e);
        } catch (Exception e) {
            throw new Exception("Error al buscar usuario '" + email + "' : " + e.getMessage());
        }
    }

    public Sucursal getSucursalCodigo(String codigo) throws UnprocessableEntityException {
        List<ErrorMessage> listaErrores = new ArrayList<>();
        try {
            return sucursalControllerFeign.getSucursalCodigo(codigo);
        } catch (FeignException e) {
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1013.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1013.getMensaje() +"-"+ e.getMessage()));
            throw new UnprocessableEntityException(listaErrores);
        }
    }


    public List<SincronizarDTO> getSincronizarEstadoCatalogo(String estado, String catalogo)  {
        List<ErrorMessage> listaErrores = new ArrayList<>();
        try {
            return sincronizarControllerFeign.getSincronizarEstadoCatalogo(estado, catalogo);
        } catch (FeignException e) {
            log.info(e.getMessage());
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1013.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1013.getMensaje() +"-"+ e.getMessage()));
            throw new UnprocessableEntityException(listaErrores);
        }
    }

    public void actualizarEstado(RequestSincronizar requestSincronizar)  {
        List<ErrorMessage> listaErrores = new ArrayList<>();
        try {
            sincronizarControllerFeign.actualizarEstado(requestSincronizar);
        } catch (FeignException e) {
            log.info(e.getMessage());
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1013.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1013.getMensaje() +"-"+ e.getMessage()));
            throw new UnprocessableEntityException(listaErrores);
        }
    }

    public void actualizarFile(RequestSincronizar requestSincronizar)  {
        List<ErrorMessage> listaErrores = new ArrayList<>();
        try {
            sincronizarControllerFeign.actualizarFile(requestSincronizar);
        } catch (FeignException e) {
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1013.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1013.getMensaje() +"-"+ e.getMessage()));
            throw new UnprocessableEntityException(listaErrores);
        }
    }

    public List<SucursalDTO> getListadoSucursal() {
        List<ErrorMessage> listaErrores = new ArrayList<>();
        try {
            return sucursalControllerFeign.getListadoSucursal();
        } catch (FeignException e) {
            log.error(e.getMessage());
            listaErrores.add(new ErrorMessage(EnumErrores.ERROR_EXCEPCION_1013.getCodigo(),
                    EnumErrores.ERROR_EXCEPCION_1013.getMensaje() +"-"+ e.getMessage()));
            throw new UnprocessableEntityException(listaErrores);
        }
    }


}
