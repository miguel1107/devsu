package com.app.commonfeign.service.inter;


import com.app.commondb.entity.Sucursal;
import com.app.commondb.model.SucursalDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "service-empresa")
public interface SucursalControllerFeign {

    @GetMapping("/sucursal/get/codigo")
    Sucursal getSucursalCodigo(@RequestParam("codigo") String codigo);

    @GetMapping("/sucursal/listar")
    List<SucursalDTO> getListadoSucursal();
}
