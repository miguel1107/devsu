package com.app.commonfeign.service.inter;

import com.app.commondb.model.RequestSincronizar;
import com.app.commondb.model.SincronizarDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "service-batch")
public interface SincronizarControllerFeign {


    @GetMapping("/sincronizar/estado/catalogo")
    List<SincronizarDTO> getSincronizarEstadoCatalogo(@RequestParam("estado") String estado, @RequestParam("catalogo") String catalogo);

    @PutMapping("/sincronizar/actualizar/estado")
    void actualizarEstado(@RequestBody  RequestSincronizar requestSincronizar);

    @PutMapping("/sincronizar/actualizar/file")
    void actualizarFile(@RequestBody RequestSincronizar requestSincronizar);
}
