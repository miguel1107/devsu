package com.app.commonfeign.service.inter;


import com.app.commondb.entity.Usuario;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service-user")
public interface UsuarioControllerFeign {

    @GetMapping("/usuario/email")
    Usuario getUserByEmail(@RequestParam("email") String email);

}
