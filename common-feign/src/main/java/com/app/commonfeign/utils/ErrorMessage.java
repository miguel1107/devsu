package com.app.commonfeign.utils;

import com.app.commondb.util.EnumErrores;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ErrorMessage implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private String codError;
    private String desError;
    public ErrorMessage(String codError, String desError) {
        this.codError = codError;
        this.desError = desError;
    }
    public ErrorMessage(EnumErrores unprocessableErrorEnum) {
        this.codError = unprocessableErrorEnum.getCodigo();
        this.desError = unprocessableErrorEnum.getMensaje();
    }

}
