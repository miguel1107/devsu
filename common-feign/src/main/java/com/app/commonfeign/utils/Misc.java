package com.app.commonfeign.utils;

import com.app.commondb.model.ErrorDTO;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.BindingResult;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class Misc {

    public static ModelMapper modelMapper = new ModelMapper();

    /**
     * Valida un objeto por sus anotaciones Bean Validation
     *
     * @param object Objeto a validar
     * @param <T>    Tipo de dato
     * @return ErrorDTO que contiene una lista con los posibles errores
     */
    public static <T> ErrorDTO validate(T object) {
        List<String> errors = new ArrayList<>();
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        try {
            Validator validator = vf.getValidator();
            Set<ConstraintViolation<T>> violations = validator.validate(object);
            if (!violations.isEmpty())
                for (ConstraintViolation<T> violation : violations)
                    errors.add("El campo '" + violation.getPropertyPath() + "' " + violation.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        } finally {
            try {
                vf.close();
            } catch (Exception e) {
            }
        }
        if (errors != null)
            if (!errors.isEmpty()) {
                ErrorDTO errorDTO = new ErrorDTO();
                errorDTO.setError(errors);
                return errorDTO;
            }
        return null;
    }

    /**
     * Concatena listas dependiendo del tipo de dato
     *
     * @param lists listas que se desean concatenar
     * @param <T>   Tipo de dato de las listas
     * @return Una sola lista que es la union de las que fueron introducidas
     */
    public static <T> List<T> concatenate(List<T>... lists) {
        Stream<T> stream = Stream.of();
        for (List<T> l : lists)
            stream = Stream.concat(stream, l.stream());
        return stream.collect(Collectors.toList());
    }

    /**
     * Crea una lista de errores por Bean Validations
     *
     * @param result   BindingResult el cual contiene la lista de errores
     * @param response Map el cual almacena parte de la informacion de retorno del mensaje
     * @return Map con lista de errores
     */
    public static Map<String, Object> listErrors(BindingResult result, Map<String, Object> response) {
        List<String> errors = result.getFieldErrors()
                .stream()
                .map(error -> "El campo '" + error.getField() + "' " + error.getDefaultMessage())
                .collect(Collectors.toList());
        response.put("error", errors);
        return response;
    }

    /**
     * Crea una lista de errores por Bean Validations
     *
     * @param result BindingResult el cual contiene la lista de errores
     * @param e      ErrorDTO el cual almacena parte de la informacion de retorno del mensaje
     * @return ErrorDTO con la lista de errores
     */
    public static ErrorDTO listErrors(BindingResult result, ErrorDTO e) {
        List<String> errors = result.getFieldErrors()
                .stream()
                .map(error -> "El campo '" + error.getField() + "' " + error.getDefaultMessage())
                .collect(Collectors.toList());
        e.setError(errors);
        return e;
    }

    /**
     * Crea un folio con formato UNIXTIME + RANDOM_NUMBERS formando un string de 20 numeros
     *
     * @return Folio con 20 numeros
     */
    public static String randomFolio() {
        String timeInSeconds = String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        String folio = timeInSeconds;
        for (int i = 0; i < 10; i++)
            folio += (String.valueOf(new Random().nextInt(9)));
        return folio;
    }

    public static String generarCodigoEvento() {

        int leftLimit = 48; // numeral '0'
        int rightLimit = 90; // letter 'z'
        int targetStringLength = 4;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1).filter(i -> (i <= 57 || i >= 65))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("ssMs");
        String datetime = ft.format(dNow);
        return generatedString + datetime;
    }

    /**
     * Convierte un LocalDateTime a Date usando ZoneId.systemDefault
     *
     * @param localDateTime Fecha a convertir
     * @return Date apartir del LocalDateTime recibido, en caso de algun error enviara
     * null e imprimira el log de error
     */
    public static Date formatLocalDateTimeToDate(LocalDateTime localDateTime) {
        try {
            return Date.from(localDateTime.atZone(ZoneId.systemDefault())
                    .toInstant());
        } catch (Exception e) {
            log.error(e);
            return null;
        }
    }

    /**
     * Convierte un Date a un LocalDateTime
     *
     * @param date Fecha en java.util.Date
     * @return LocalDateTime apartir el date
     */
    public static LocalDateTime formatLocalDateTime(Date date) {
        try {
            return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        } catch (Exception e) {
            log.error(e);
            return null;
        }
    }

    /**
     * Convirte un String en formato yyyy-MM-dd'T'HH:mm:ss a un LocalDateTime
     *
     * @param date Fecha en String que se desea convertir
     * @return LocalDateTime a partir del String, en caso de algun error enviara null e imprimira el log de error
     */
    public static LocalDateTime formatLocalDateTime(String date) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            return LocalDateTime.parse(date, formatter);
        } catch (Exception e) {
            log.error(e);
            return null;
        }
    }

    /**
     * Extrae el día de la semana del LocalDateTime y lo devuelve en mayusculas sin acentos
     *
     * @param dateTime LocalDateTime al que se le extraera el dia de la semana
     * @return String con el día de la semana, Ej. LUNES
     */
    public static String dayOfWeek(LocalDateTime dateTime) {
        return Misc.stripAccents(
                dateTime.getDayOfWeek().getDisplayName(
                                TextStyle.FULL, Locale.forLanguageTag("es-ES"))
                        .toUpperCase(Locale.ROOT));
    }

    /**
     * Suma minutos a un objeto java.sql.Time
     *
     * @param t   Time al cual se le suamaran los minutos
     * @param min Integer con el numero de minutos a sumar
     * @return java.sql.Time con los minutos ya agregados
     */
    public static Time plusMinutes(Time t, Integer min) {
        return Time.valueOf(t.toLocalTime().plusMinutes(min));
    }

    /**
     * Elimina los acentos de un String
     *
     * @param s String al cual se le eliminaran los acentos
     * @return String formateado sin acentos, salvando la letra ñ
     */
    public static String stripAccents(String s) {
        /*Salvamos las ñ*/
        s = s.replace('ñ', '\001');
        s = s.replace('Ñ', '\002');
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        /*Volvemos las ñ a la cadena*/
        s = s.replace('\001', 'ñ');
        s = s.replace('\002', 'Ñ');
        return s;
    }

    /**
     * Convierte un Object a BigDecimal, dando la posibilidad de enviar el numero entero en el Object y que este
     * devuelva el numero con .00 para evitar problemas si estos llegan a faltar en el Object enviado
     *
     * @param value Object el cual se convertira
     * @return BigDecimal apartir de Object
     */
    public static BigDecimal matToBigDecimal(Object value) {
        BigDecimal result = new BigDecimal(0);
        if (value != null) {
            try {
                if (value instanceof BigDecimal) {
                    result = (BigDecimal) value;
                } else if (value instanceof String) {
                    result = new BigDecimal((String) value);
                } else if (value instanceof BigInteger) {
                    result = new BigDecimal((BigInteger) value);
                } else if (value instanceof Number) {
                    result = new BigDecimal(((Number) value).doubleValue());
                } else {
                    //throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal.");
                    log.error("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal.");
                }
            } catch (NumberFormatException e) {
                log.error("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal. " + e);
            } catch (ClassCastException e) {
                log.error("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal. " + e);
            } catch (Exception e) {
                log.error("Exception caught. " + e);
            }
        }
        return result;
    }

    /**
     * Suma N cantidad de meses a un objeto java.util.Date
     *
     * @param date java.util.Date el cual se le suamara
     * @param i    cantidad a sumar
     * @return java.util.Date con los meses ya añadidos
     */
    public static Date addMonth(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, i);
        return cal.getTime();
    }

    /**
     * Inicializa la paginacion con Pageable y los parametros necesarios
     *
     * @param requestParams Contiene page, size, sortBy, direction que formaran el PageRequest
     *                      que construye el objeto Pageable.
     *                      Page es el número de pagina que se desea consultar.
     *                      Size es el tamaño de la pagina o cantidad de elementos que se desean ver por pagina.
     *                      SortBy contiene el objeto que se desea ordenar por JPQL.
     *                      Direction puede se asc (ascendente) o desc (descendente).
     * @return Pageable formado por numero de pagina, tamaño de pagina, orden de elementos, direccion de orden
     */
    public static Pageable initPagination(Map<String, Object> requestParams) {
        Pageable pageable = null;
        Integer page = Integer.parseInt(requestParams.get("page").toString());
        Integer size = Integer.parseInt(requestParams.get("size").toString());
        String attribute = (String) requestParams.get("sortBy");
        String order = (String) requestParams.get("direction");

        if (order.equals("asc"))
            pageable = PageRequest.of(page, size, Sort.by(attribute).ascending());
        else if (order.equals("desc"))
            pageable = PageRequest.of(page, size, Sort.by(attribute).descending());
        return pageable;
    }

    public static Sort sortByValue(Map<String, Object> requestParams) {
        String attribute = (String) requestParams.get("sortBy");
        String order = (String) requestParams.get("direction");
        if (order.equals("asc") && !attribute.isEmpty()){
            return Sort.by(Sort.Direction.ASC, attribute);
        }else if(order.equals("desc") && !attribute.isEmpty()){
            return Sort.by(Sort.Direction.DESC, attribute);
        }else {
            return Sort.by(Sort.Direction.ASC, "key");
        }
    }

}
