package com.app.commonfeign.utils;

/**
 * @author Jorge Guerrero Silva
 * @since 20/12/2020
 */
public abstract class Roles {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_JUECES = "ROLE_JUECES";
    public static final String ROLE_ORGANIZADOR = "ROLE_ORGANIZADOR";
    public static final String ROLE_CAPITANAS = "ROLE_CAPITANAS";
    public static final String ROLE_ENTRENADORES = "ROLE_ENTRENADORES";
    public static final String ROLE_LOCUTOR = "ROLE_LOCUTOR";

}
