package com.app.commonfeign.utils;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;


@Getter
@Setter
public class ErrorResponse {

    @JsonProperty("Success")
    private Boolean Success;

    @JsonProperty("Code")
    private Integer code;

    @JsonProperty("IdRequest")
    private UUID idRequest;

    @JsonProperty("Messages")
    private List<ErrorMessage> messages;

    @JsonProperty("Data")
    private List<Object> data;

}
