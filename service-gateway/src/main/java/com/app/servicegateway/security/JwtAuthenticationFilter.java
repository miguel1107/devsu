package com.app.servicegateway.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtException;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Component
public class JwtAuthenticationFilter implements WebFilter {

    @Autowired
    private ReactiveAuthenticationManager authenticationManager;
    public HashMap<String, Object> response = new HashMap<>();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        return Mono.justOrEmpty(exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION))
                .filter(authHeader -> authHeader.startsWith("Bearer "))
                .switchIfEmpty(chain.filter(exchange).then(Mono.empty()))
                .map(token -> token.replace("Bearer ", ""))
                .flatMap(token -> authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(null, token)))
                .flatMap(authentication -> chain.filter(exchange).contextWrite(ReactiveSecurityContextHolder.withAuthentication(authentication)))
                .onErrorResume(JwtException.class, e -> {
                    byte[] a = null;
                    ObjectMapper objectMapper = new ObjectMapper();
                    List<String> messages = new ArrayList<>();
                    messages.add("Invalid or Expired Token");
                    response.put("Success", false);
                    response.put("Code", HttpStatus.UNAUTHORIZED.value());
                    response.put("IdRequest", null);
                    response.put("Messages", messages);
                    response.put("Data", null);
                    try {
                     a =   objectMapper.writeValueAsBytes(response);
                    } catch (JsonProcessingException ex) {
                        throw new RuntimeException(ex);
                    }
                    DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(a);
                    return Mono.fromRunnable(() -> {
                        ServerHttpResponse resp = exchange.getResponse();
                        resp.getHeaders().add(HttpHeaders.CONTENT_TYPE, String.valueOf(MediaType.APPLICATION_JSON));
                        resp.setStatusCode(HttpStatus.UNAUTHORIZED);
                    }).then(exchange.getResponse().writeWith(Flux.just(buffer)));
                })
                .onErrorResume(AuthenticationException.class, e -> {
                    byte[] a = null;
                    ObjectMapper objectMapper = new ObjectMapper();
                    List<String> messages = new ArrayList<>();
                    messages.add("Bad credentials");
                    response.put("Success", false);
                    response.put("Code", HttpStatus.UNAUTHORIZED.value());
                    response.put("IdRequest", null);
                    response.put("Messages", messages);
                    response.put("Data", null);
                    try {
                        a =   objectMapper.writeValueAsBytes(response);
                    } catch (JsonProcessingException ex) {
                        throw new RuntimeException(ex);
                    }
                    DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(a);
                    return Mono.fromRunnable(() -> {
                        ServerHttpResponse resp = exchange.getResponse();
                        resp.getHeaders().add(HttpHeaders.CONTENT_TYPE, String.valueOf(MediaType.APPLICATION_JSON));
                        resp.setStatusCode(HttpStatus.UNAUTHORIZED);
                    }).then(exchange.getResponse().writeWith(Flux.just(buffer)));
                });


    }

}
