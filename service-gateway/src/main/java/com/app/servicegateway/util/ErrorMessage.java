package com.app.servicegateway.util;


import java.io.Serializable;


public class ErrorMessage implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private String codError;
    private String desError;
    public ErrorMessage(String codError, String desError) {
        this.codError = codError;
        this.desError = desError;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public String getDesError() {
        return desError;
    }

    public void setDesError(String desError) {
        this.desError = desError;
    }
}
