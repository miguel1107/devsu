package com.app.servicegateway.util;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.UUID;


public class ErrorResponse {

    @JsonProperty("Success")
    private Boolean Success;

    @JsonProperty("Code")
    private Integer code;

    @JsonProperty("IdRequest")
    private UUID idRequest;

    @JsonProperty("Messages")
    private List<ErrorMessage> messages;

    @JsonProperty("Data")
    private List<Object> data;

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public UUID getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(UUID idRequest) {
        this.idRequest = idRequest;
    }

    public List<ErrorMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ErrorMessage> messages) {
        this.messages = messages;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }
}
