package com.app.commondb.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "facturas")
public class Factura {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(updatable = false, nullable = false, columnDefinition = "uuid DEFAULT uuid_generate_v4()")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID key;

    private String sentido;

    @JoinColumn(name = "sucursal_key")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Sucursal sucursal;

    private LocalDateTime fechaRegistro;
    private Integer procesado;

    private LocalDateTime fechaProceso;

    @Column(columnDefinition="TEXT")
    private String contenido;

    @Column(name = "ind_del", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean indDel;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdAt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updatedAt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime deleteAt;

    private String createdBy;

    private String updatedBy;

    private String deleteBy;


    @PrePersist
    public void createdAtAuto() {
        createdAt = LocalDateTime.now();
    }

    @PreUpdate
    public void updatedAtAuto() {
        updatedAt = LocalDateTime.now();
    }
}
