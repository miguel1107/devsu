package com.app.commondb.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "cuenta")
public class Cuenta implements Serializable {

        private static final long serialVersionUID = 1L;

        @Id
        @Column(updatable = false, nullable = false, columnDefinition = "uuid DEFAULT uuid_generate_v4()")
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID key;
        private String numeroCuenta;
        private String tipoCuenta;
        private Double saldoInicial;
        private String estado;

        @Column(name = "ind_del", columnDefinition = "BOOLEAN DEFAULT false")
        private Boolean indDel;

        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime createdAt;

        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime updatedAt;

        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime deleteAt;

        private String createdBy;

        private String updatedBy;

        private String deleteBy;


        @PrePersist
        public void createdAtAuto() {
                createdAt = LocalDateTime.now();
        }

        @PreUpdate
        public void updatedAtAuto() {
                updatedAt = LocalDateTime.now();
        }


}
