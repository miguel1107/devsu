package com.app.commondb.entity;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "logs")
public class Log implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(updatable = false, nullable = false, columnDefinition = "uuid DEFAULT uuid_generate_v4()")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID key;

    private UUID idRequest;

    private Integer SAPDocId;

    @JoinColumn(name = "sucursal_key")
    @ManyToOne(fetch = FetchType.LAZY)
    private Sucursal sucursal;

    private Integer codigoRespuesta;

    private String mensajeRespuesta;

    private LocalDateTime dateTime;

    private Integer estatus;

    private String endpoint;

    private String apiRoute;

    private String keyToken;

    private String valueToken;

    private String method;

    private Integer tipo;

    private Integer intentos;

    private String codigoRespuestaWebhook;

    private String mensajeRespuestaWebhook;

    @Column(name = "ind_del", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean indDel;

    @Embedded
    private Auditoria auditoria;



}
