package com.app.commondb.util;


public enum EnumErrores {

    ERROR_EXCEPCION_1001("1001", "Invalid Sucursal UUID"),

    ERROR_EXCEPCION_1002("1002", "Sucursal no existe"),

    ERROR_EXCEPCION_1003("1003", "Sucursal no registrada"),

    ERROR_EXCEPCION_1004("1004", "Object cannot be null or empty"),

    ERROR_EXCEPCION_1005("1005", "Endpoint cannot be null or empty"),

    ERROR_EXCEPCION_1006("1006", "ApiRoute cannot be null or empty"),

    ERROR_EXCEPCION_1007("1007", "ApiToken cannot be null or empty"),

    ERROR_EXCEPCION_1008("1008", "ApiToken-Key cannot be null or empty"),

    ERROR_EXCEPCION_1009("1009", "ApiToken-Value cannot be null or empty"),

    ERROR_EXCEPCION_1010("1010", "Receiver cannot be null or empty"),

    ERROR_EXCEPCION_1011("1011", "Object-data cannot be null or empty"),

    ERROR_EXCEPCION_1012("1012", "Failed to find sucursal"),

    ERROR_EXCEPCION_1013("1013", "Feign Client Exception"),

    ERROR_EXCEPCION_1014("1014", "Error cliente webhook"),

    ERROR_EXCEPCION_1015("1015", "Seleccione un tipo de pago"),

    ERROR_EXCEPCION_1016("1016", "El tipo de pago ya se encuentra registrado"),

    ERROR_EXCEPCION_1017("1017", "Ingrese un numero de cuenta"),

    ERROR_EXCEPCION_1018("1018", "Error al procesar el log"),

    ERROR_EXCEPCION_1019("1019", "Object cannot be null or empty"),

    ERROR_EXCEPCION_1020("1020", "Connector cannot be null or empty"),

    ERROR_EXCEPCION_1021("1021", "Company cannot be null or empty"),

    ERROR_EXCEPCION_1022("1022", "TaxId cannot be null or empty"),

    ERROR_EXCEPCION_1023("1023", "BranchOffice cannot be null or empty"),

    ERROR_EXCEPCION_1024("1024", "Name cannot be null or empty"),

    ERROR_EXCEPCION_1025("1025", "Aún falta terminar de configurar sus cuentas"),


    ERROR_EXCEPCION_1026("1026", "Sucursal Invalido"),

    ERROR_EXCEPCION_1027("1027", "Sucursal no registrado"),

    ERROR_EXCEPCION_1028("1028", "Cuenta no registrada"),

    ERROR_EXCEPCION_1029("1029", "Error al subir al subir archivo"),

    ERROR_EXCEPCION_1030("1030", "El archivo no puede estar vacío"),
    ERROR_EXCEPCION_1031("1031", "Catalogo invalido"),

    ERROR_EXCEPCION_1032("1032", "El catalogo que desea sincronizar no existe"),

    ERROR_EXCEPCION_1033("1033", "No hay ningun registro pendiente de procesar"),

    ERROR_EXCEPCION_1034("1034", "Error al invocar el proceso batch"),

    ERROR_EXCEPCION_1035("1035", "Error al cargar archivo"),
    ERROR_EXCEPCION_1036("1036", "Error al solicitar sincronizaciones por estado ");


    private EnumErrores(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCodigo() {
        return code;
    }

    public String getMensaje() {
        return msg;
    }

    public static String getMensaje(String code) {
        String msg = "";
        for (EnumErrores enumHTTP : EnumErrores.values()) {
            if (enumHTTP.code.equalsIgnoreCase(code)) {
                msg = enumHTTP.msg;
                break;
            }
        }
        return msg;
    }
}
