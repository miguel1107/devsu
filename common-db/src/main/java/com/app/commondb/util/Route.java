package com.app.commondb.util;

public abstract class Route {

    public static final String U_UPLOADS_IMAGES = "/uploads/";
    public static final String W_UPLOADS_IMAGES = "C://uploads//";
    public static final String U_SUNAT_XML = "/sunat/xml/";
    public static final String W_SUNAT_XML = "C://sunat//xml//";
    public static final String U_SUNAT_CDR = "/sunat/respuesta/";
    public static final String W_SUNAT_CDR = "C://sunat//respuesta//";
    public static final String U_CERTIFICADO = "/certificado/";
    public static final String W_CERTIFICADO = "C://certificado//";
    public static final String U_RESPUESTA = "/sunat/respuesta/";
    public static final String W_RESPUESTA = "C://sunat//respuesta//";
    public static final String U_SUNAT_XML_PATH = "/sunat/xml";
    public static final String W_SUNAT_XML_PATH = "C://sunat//xml";
    public static final String U_SUNAT_CDR_PATH = "/sunat/respuesta";
    public static final String W_SUNAT_CDR_PATH = "C://sunat//respuesta";

    public static final String U_COTIZACION_PDF = "/pdf/cotizacion/";
    public static final String W_COTIZACION_PDF = "C://pdf//cotizacion//";

    public static final String U_FACTURA_PDF = "/pdf/factura/";
    public static final String W_FACTURA_PDF = "C://pdf//factura//";

    public static final String U_BOLETA_PDF = "/pdf/boleta/";
    public static final String W_BOLETA_PDF = "C://pdf//boleta//";

    public static final String U_GUIA_REMISION_PDF = "/pdf/guia/";
    public static final String W_GUIA_REMISION_PDF = "C://pdf//guia//";

    public static final String U_NOTA_CREDITO_PDF = "/pdf/nc/";
    public static final String W_NOTA_CREDITO_PDF = "C://pdf//nc//";

    public static final String U_NOTA_DEBITO_PDF = "/pdf/nd/";
    public static final String W_NOTA_DEBITO_PDF = "C://pdf//nd//";

}
