package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemModel {
	
	private String codigo;
	private String nombre;
	private String tipo;
	private String descripcion;
	private String unidadMedida;//es otra clase
	private String categoriaGasto;//es otra clase

}
