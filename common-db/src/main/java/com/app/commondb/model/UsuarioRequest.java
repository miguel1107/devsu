package com.app.commondb.model;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
public class UsuarioRequest {

    private UUID key;

    private String curp;

    private String sexo;

    private String nombre;

    private String apellidoPaterno;

    private String apellidoMaterno;

    private LocalDate fechaNacimiento;

    private String ocupacion;

    private String estadoCivil;

    private String email;

    private String password;

    private String photo;

    private String ciudad;

    private String direccion;

    private String CP;

    private String telefono;

    private List<RolDTO> roles;

    private Boolean enable;

    private String resena;
}
