package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RequestSucursal {


    private UUID key;
    private String clave;
    private String nombre;
    //private Integer tipoInterfazSap;

}
