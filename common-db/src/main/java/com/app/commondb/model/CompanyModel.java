package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyModel {

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("TaxId")
    private String TaxId;

    @JsonProperty("BranchOffice")
    private String BranchOffice;




}
