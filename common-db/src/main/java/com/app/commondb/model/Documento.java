package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class Documento {

    private String catalogo;
    private LocalDateTime fecha;
    private FileZIP resource;
    private Integer totalRegistros;
    private UUID keySincronizar;
}
