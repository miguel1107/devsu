package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcesoSucursal {

    private String file;
    private Integer totalRegistros;
}
