package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RequestListCuentas {

    private List<CuentaDTO> listaCuentas;

    private String key;


    public RequestListCuentas() {
        listaCuentas = new ArrayList<CuentaDTO>();
    }
}
