package com.app.commondb.model;

import lombok.Data;

import java.util.UUID;

@Data
public class RolDTO {
    private UUID key;
    private String name;
    private String descripcion;
    private Boolean enable;

}
