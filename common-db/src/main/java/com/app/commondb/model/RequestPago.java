package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestPago {

    @JsonProperty("SucursalKey")
    private String SucursalKey;

    @JsonProperty("Receiver")
    private Receiver receiver;

    @JsonProperty("Object")
    private ObjectRequest object;


}

