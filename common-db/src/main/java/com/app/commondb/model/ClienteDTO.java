package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ClienteDTO {

    private String key;
    private String password;
    private String estado;
}
