package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerModel {

    private String codigo;
    private String nombre;
    private String apellido;
    private String telefono;
    private String empresa;
    private String celular;
    private String direccion;
    private String email;

}
