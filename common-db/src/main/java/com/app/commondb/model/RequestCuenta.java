package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RequestCuenta {

    private UUID key;
    private TipoPagoDTO tipoPago;
    private String sucursalKey;
    private String numero;
    private Boolean enable;
}
