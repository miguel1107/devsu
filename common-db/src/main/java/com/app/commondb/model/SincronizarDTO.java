package com.app.commondb.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class SincronizarDTO {

    private UUID key;

    private LocalDateTime fechaPeticion;

    private LocalDateTime fechaProcesado;

    private LocalDateTime fechaSincronizacion;

    private LocalDateTime fechaFinalizacion;

    private String estado;

    private String file;

    private SucursalDTO sucursal;

    private CatalogoDTO catalogo;

    private Boolean enable;

    private String endpoint;

    private String apiRoute;

    private String keyToken;

    private String valueToken;

    private String method;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createdAt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updatedAt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime deleteAt;

    private String createdBy;

    private String updatedBy;

    private String deleteBy;

}
