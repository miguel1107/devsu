package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RequestSincronizacion {

    private UUID key;
    private String sucursalKey;
    private String catalogo;
    private Boolean enable;
    private String endPoint;
    private String apiRoute;
    private String keyToken;
    private String valueToken;
    private String method;
}
