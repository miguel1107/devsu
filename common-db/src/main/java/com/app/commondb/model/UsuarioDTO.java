package com.app.commondb.model;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
public class UsuarioDTO {

    private UUID key;

    private String curp;

    private String sexo;

    private String nombre;

    private String apellidoPaterno;

    private String apellidoMaterno;

    private LocalDate fechaNacimiento;

    private String ocupacion;

    private String estadoCivil;

    private String email;

    private String photo;

    private String ciudad;

    private String direccion;

    private String CP;

    private String telefono;

    private String resena;

    private List<RolDTO> roles;

    private Boolean enable;

}
