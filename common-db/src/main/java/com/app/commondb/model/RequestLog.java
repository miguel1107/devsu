package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RequestLog {

    @JsonProperty("IdRequest")
    private UUID IdRequest;
    @JsonProperty("SucursalKey")
    private UUID SucursalKey;
    @JsonProperty("CodigoRespuesta")
    private Integer CodigoRespuesta;
    @JsonProperty("MensajeRespuesta")
    private String MensajeRespuesta;

    @JsonProperty("Endpoint")
    private String endpoint;

    @JsonProperty("ApiRoute")
    private String apiRoute;

    @JsonProperty("KeyToken")
    private String keyToken;

    @JsonProperty("ValueToken")
    private String valueToken;

    @JsonProperty("Method")
    private String method;

    @JsonProperty("Estatus")
    private Integer Estatus;

    @JsonProperty("Tipo")
    private Integer Tipo;
}
