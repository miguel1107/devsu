package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class EmpresaDTO {

    private String key;
    private String clave;
    private String rfc;
    private String nombre;
    private List<SucursalDTO> sucursals;
    private Boolean enable;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime deleteAt;
    private String createdBy;
    private String updatedBy;
    private String deleteBy;
}
