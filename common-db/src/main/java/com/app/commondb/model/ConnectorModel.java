package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConnectorModel {

//    @JsonProperty("Type")
//    private String Type;

    @JsonProperty("Company")
    private CompanyModel Company;

//    @JsonProperty("Url")
//     private String Url;
//
//    @JsonProperty("Credentials")
//    private CredentialsModel Credentials;


}
