package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class ResponsePago {

    @JsonProperty("Key")
    private UUID key;

    @JsonProperty("FechaRegistro")
    private LocalDateTime fechaRegistro;

    @JsonProperty("Procesado")
    private Integer procesado;

    @JsonProperty("Object")
    private ObjectRequest object;

}
