package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiToken {

    @JsonProperty("Key")
    private String key;

    @JsonProperty("Value")
    private String value;

}
