package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Receiver {

    @JsonProperty("Endpoint")
    private String endpoint;

    @JsonProperty("ApiRoute")
    private String apiRoute;

    @JsonProperty("ApiToken")
    private ApiToken apiToken;

    @JsonProperty("Method")
    private String method;



}
