package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter

public class MovimienDTO {

    private String key;
    private Date fecha;
    private String tipoMovimiento;
    private Double valor;
    private Double saldo;
}
