package com.app.commondb.model;

import lombok.Data;

@Data
public class ErrorDTO {
    private String message;
    private Object error;
}
