package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestEmpresa {

//    @JsonProperty("SyncEntity")
//    private String SyncEntity;
//    @JsonProperty("Action")
//    private String Action;

    @JsonProperty("Receiver")
    private Receiver Receiver;
    @JsonProperty("Object")
    private ObjectModel objectModel;



}
