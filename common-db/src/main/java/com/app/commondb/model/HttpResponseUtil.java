package com.app.commondb.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class HttpResponseUtil implements Serializable {
        private static final long serialVersionUID = 1L;

        private int codigo;
        private String message;
        private String status;
        private Object data;

	public HttpResponseUtil() {
        }
	public HttpResponseUtil(int codigo, String message, String status) {
            super();
            this.codigo = codigo;
            this.message = message;
            this.status = status;
        }

	public HttpResponseUtil(int codigo, String message, String status, Object errors) {
            super();
            this.codigo = codigo;
            this.message = message;
            this.status = status;
        }

}
