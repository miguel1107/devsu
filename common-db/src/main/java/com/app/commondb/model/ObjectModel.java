package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjectModel {

    @JsonProperty("Connector")
    private ConnectorModel connector;



}
