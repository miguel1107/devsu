package com.app.commondb.model.sales;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteModel {


    private String codigo;
    private String codigoExterno;
    private String apellido;
    private String nombre;
    private String telefono;
    private String email;
    private String celular;
    private String numDocumento;
    private String razonSocial;
    private String direccion;
    private String fechaNacimiento;




}
