package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileZIP {

    private String nameFile;
    private Long tamanio;
    private String content;


}
