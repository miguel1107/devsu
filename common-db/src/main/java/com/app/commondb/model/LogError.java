package com.app.commondb.model;

import lombok.Data;

import java.time.LocalDate;

/**
 * @author Carlos Montoya
 * @since 04/07/2019
 */
@Data
public class LogError
{
	private int id;
	private int clienteId;
	private String tipoDocumento;
	private String nombre;
	private String mensaje;
	private String contenido;
	private LocalDate fecha;
}
