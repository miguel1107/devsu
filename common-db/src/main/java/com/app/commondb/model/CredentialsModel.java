package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CredentialsModel {

    @JsonProperty("Database")
    private String Database;
    @JsonProperty("User")
    private String User;
    @JsonProperty("Pass")
    private String Pass;
}
