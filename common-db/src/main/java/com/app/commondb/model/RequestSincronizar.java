package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RequestSincronizar {

    private String key;
    private String estado;
    private String file;
}
