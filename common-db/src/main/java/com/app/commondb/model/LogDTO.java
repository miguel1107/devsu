package com.app.commondb.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class LogDTO {

    private UUID key;
    private UUID idRequest;
    private Integer SAPDocId;
    private RequestSucursal sucursal;
    private Integer codigoRespuesta;
    private String mensajeRespuesta;
    private LocalDateTime dateTime;
    private Integer estatus;
    private String endpoint;
    private String apiRoute;
    private String keyToken;
    private String valueToken;
    private String method;
    private Integer tipo;

    private Boolean enable;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime deleteAt;
    private String createdBy;
    private String updatedBy;
    private String deleteBy;

}
