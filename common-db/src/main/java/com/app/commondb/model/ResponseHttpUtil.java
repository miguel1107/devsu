package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class ResponseHttpUtil {

    @JsonProperty("success")
    private Boolean success;

    @JsonProperty("code")
    private Integer code;

    @JsonProperty("idRequest")
    private UUID idRequest;

    @JsonProperty("messages")
    private List<String> messages;

    @JsonProperty("data")
    private Object data;

    public ResponseHttpUtil() {
    }

    public ResponseHttpUtil(Boolean success, Integer code, UUID idRequest, List<String> messages, Object data) {
        this.success = success;
        this.code = code;
        this.idRequest = idRequest;
        this.messages = messages;
        this.data = data;
    }
}

