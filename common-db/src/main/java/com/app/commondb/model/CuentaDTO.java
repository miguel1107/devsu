package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuentaDTO {

    private String key;
    private String numerCuenta;
    private String tipoCuenta;
    private Double saldoInicial;
    private String estado;
}
