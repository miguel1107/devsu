package com.app.commondb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestFactura {

    @JsonProperty("SucursalKey")
    private String SucursalKey;

    @JsonProperty("Receiver")
    private Receiver receiver;

    @JsonProperty("Object")
    private ObjectRequest object;

}
