package com.app.commondb.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
public class SucursalDTO {

    private UUID key;
    private String clave;
    private String codigo;
    private String codigoExterno;
    private String nombre;
    private Boolean cuentasVerificada;
    private Boolean enable;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime deleteAt;
    private String createdBy;
    private String updatedBy;
    private String deleteBy;
}
