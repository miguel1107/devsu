package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class TipoTransaccionDTO {

    private UUID key;
    private String nombre;
    private String clave;
    private Boolean enable;
}
