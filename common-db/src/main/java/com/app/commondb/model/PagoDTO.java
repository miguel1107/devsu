package com.app.commondb.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class PagoDTO {

    private UUID key;
    private String sentido;
    private SucursalDTO sucursal;
    private LocalDateTime fechaRegistro;
    private Integer procesado;
    private LocalDateTime fechaProceso;
    private String contenido;
    private Boolean enable;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime deleteAt;
    private String createdBy;
    private String updatedBy;
    private String deleteBy;
}
