package com.app.commondb.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommonServiceInter <E, D, X extends Throwable> {

    public List<D> findAll(Specification<E> specs, Sort sort);

    public Page<D> findAll(Specification<E> specs, Pageable pageable);

    public Optional<D> findById(UUID key);

    public D mapToClassDTO(E e);

    public E mapToClass(D d);
}

